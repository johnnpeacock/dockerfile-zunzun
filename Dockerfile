FROM ubuntu:latest
MAINTAINER John Peacock <johnnpeacock@gmail.com>
RUN apt-get update && apt-get install -y python-scipy python-matplotlib python-django python-beautifulsoup python-reportlab python-reportlab-accel imagemagick gifsicle python-pip git nano
RUN pip install pyeq2
RUN git clone https://bitbucket.org/zunzuncode/zunzunsite.git
COPY ./zunzun.sh /
ENTRYPOINT ["/zunzun.sh"]
EXPOSE 8000